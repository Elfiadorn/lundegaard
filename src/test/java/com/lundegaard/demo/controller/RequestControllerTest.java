package com.lundegaard.demo.controller;

import com.lundegaard.demo.controller.config.RequestControllerContextConfiguration;
import com.lundegaard.demo.service.RequestTypeService;
import lombok.NoArgsConstructor;
import lombok.var;
import org.apache.commons.collections4.IterableUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@NoArgsConstructor
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RequestControllerContextConfiguration.class)
public class RequestControllerTest {

	@Autowired
	private RequestTypeService requestTypeService;

	@Test
	public void retrieveRequestTypes() {
		var result = requestTypeService.getAllTypes();
		Assert.assertEquals("Size is incorrect", 3, IterableUtils.size(result));
	}
}

