package com.lundegaard.demo.controller;

import com.google.common.collect.Lists;
import com.lundegaard.demo.controller.config.FormControllerContextConfiguration;
import com.lundegaard.demo.model.FormEntity;
import com.lundegaard.demo.model.RequestType;
import com.lundegaard.demo.service.FormEntityService;
import lombok.NonNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = FormControllerContextConfiguration.class)
public class FormControllerTest {

	@Autowired
	private FormEntityService formEntityService;

	@Test
	public void saveForm() {
		for (FormEntity entity : getEntityParameters()) {
			formEntityService.saveFormEntity(entity);
		}
	}

	@Test
	public void handleException() {
		for (Exception exception : getExceptionParameter()) {
			Assert.notNull(exception, "Exception cannot be null");
		}
	}

	@NonNull
	private List<Exception> getExceptionParameter() {
		return Lists.newArrayList(
				new NullPointerException("There is null pointer")
		);
	}

    @NonNull
	private List<FormEntity> getEntityParameters() {
		new RequestType(UUID.randomUUID(), "TYPE");

		return Lists.newArrayList(
				new FormEntity(
						UUID.randomUUID(),
						new RequestType(UUID.randomUUID(), "TYPE"),
						1,
						"Name",
						"Surname",
						"Request"
				)
		);
	}

}
