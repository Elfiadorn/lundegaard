package com.lundegaard.demo.controller.config;

import com.lundegaard.demo.dao.FormEntityDAO;
import com.lundegaard.demo.model.FormEntity;
import com.lundegaard.demo.service.FormEntityService;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import static org.mockito.Mockito.when;

@TestConfiguration
public class FormControllerContextConfiguration {

	@InjectMocks
	private FormEntity entity;

	@MockBean
	private FormEntityDAO formEntityDAO;

	@Bean
	public FormEntityService formEntityService() {
		FormEntityService formEntityService = new FormEntityService(formEntityDAO);
		when(formEntityDAO.save(entity)).thenReturn(entity);
		return formEntityService;
	}

}
