package com.lundegaard.demo.controller.config;

import com.lundegaard.demo.dao.RequestTypeDAO;
import com.lundegaard.demo.model.RequestType;
import com.lundegaard.demo.service.RequestTypeService;
import org.assertj.core.util.Lists;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import java.util.UUID;

@TestConfiguration
public class RequestControllerContextConfiguration {

    @MockBean
    private RequestTypeDAO requestTypeDAO;

    @Bean
    public RequestTypeService requestTypeService() {
        RequestTypeService requestTypeService = new RequestTypeService(requestTypeDAO);

        Iterable<RequestType> types = Lists.newArrayList(
                new RequestType(UUID.randomUUID(), "a"),
                new RequestType(UUID.randomUUID(), "b"),
                new RequestType(UUID.randomUUID(), "c")
        );

        Mockito.when(requestTypeService.getAllTypes())
                .thenReturn(types);

        return requestTypeService;
    }

}
