package com.lundegaard.demo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class FormEntity implements Serializable {

    @Id
    @Nullable
    @GeneratedValue
    private UUID id;

    @NonNull
    @ManyToOne()
    @JoinColumn(foreignKey = @ForeignKey(name = "request_type_fk"))
    private RequestType requestType;

    @NonNull
    private Integer policyNumber;

    @NonNull
    @NotNull
    @Length(max = 50)
    private String name;

    @NonNull
    @NotNull
    @Length(max = 50)
    private String surname;

    @NonNull
    @NotNull
    @Length(max = 500)
    private String request;

    @Override
    public int hashCode() {
        return Objects.hash(id, requestType, policyNumber, name, surname, request);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final FormEntity other = (FormEntity) obj;
        return Objects.equals(this.id, other.id)
                && Objects.equals(this.requestType, other.requestType)
                && Objects.equals(this.policyNumber, other.policyNumber)
                && Objects.equals(this.name, other.name)
                && Objects.equals(this.surname, other.surname)
                && Objects.equals(this.request, other.request);
    }
}
