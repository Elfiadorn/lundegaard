package com.lundegaard.demo.model.exception;

import com.lundegaard.demo.model.FormEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ValidationException extends Throwable {

    private final Set<ConstraintViolation<FormEntity>> violations;

    // TODO this can be polished better or other way - mkorcak
    /**
     * Return all violations in http header, under key Errors
     *
     * @return ReponseEntity where we put all violations messages
     */
    @NonNull
    public ResponseEntity getResponseEntity() {
        List<String> collect = violations.stream()
                .map(violation ->
                        (!Objects.isNull(violation.getPropertyPath()) ? violation.getPropertyPath().toString() : "")
                                + " - " + violation.getMessage()
                )
                .collect(Collectors.toList());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("Errors", collect);

        return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
    }

}
