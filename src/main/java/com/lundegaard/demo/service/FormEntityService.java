package com.lundegaard.demo.service;

import com.lundegaard.demo.dao.FormEntityDAO;
import com.lundegaard.demo.model.FormEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FormEntityService {

    private final FormEntityDAO formEntityDAO;

    /**
     * Save form entity to db
     *
     * @param entity - data model
     */
    public void saveFormEntity(@NonNull final FormEntity entity) {
        formEntityDAO.save(entity);
    }

}
