package com.lundegaard.demo.service;

import com.lundegaard.demo.dao.RequestTypeDAO;
import com.lundegaard.demo.model.RequestType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RequestTypeService {

    private final RequestTypeDAO requestTypeDAO;

    /**
     * @return all request types from db
     */
    @NonNull
    public Iterable<RequestType> getAllTypes() {
        return requestTypeDAO.findAll();
    }

}
