package com.lundegaard.demo.dao;

import com.lundegaard.demo.model.RequestType;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface RequestTypeDAO extends CrudRepository<RequestType, UUID> {
}
