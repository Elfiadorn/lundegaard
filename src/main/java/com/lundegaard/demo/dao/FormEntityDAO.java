package com.lundegaard.demo.dao;

import com.lundegaard.demo.model.FormEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

/**
 * DAO FormEntity - represents connection with db
 * @see FormEntity
 */
public interface FormEntityDAO extends CrudRepository<FormEntity, UUID> {
}
