package com.lundegaard.demo.controller;

import com.lundegaard.demo.model.FormEntity;
import com.lundegaard.demo.model.exception.ValidationException;
import com.lundegaard.demo.service.FormEntityService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

@Slf4j
@RestController
@RequestMapping("/form")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FormController {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private final FormEntityService formEntityService;

    /**
     * url - host:port/form
     * method - post
     * @param entity - object to save, format JSON
     * @return Response entity with OK status or with BAD_REQUEST (included reasons)
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity saveForm(@RequestBody @NonNull final FormEntity entity) {
        log.info("Saving form");
        Set<ConstraintViolation<@NonNull FormEntity>> violations = validator.validate(entity);

        if (CollectionUtils.isEmpty(violations)) {
            formEntityService.saveFormEntity(entity);
            log.info("HERE");
            return new ResponseEntity(HttpStatus.OK);
        } else {
            log.info("Saving form failed with {} violations", violations.size());
            return new ValidationException(violations).getResponseEntity();
        }
    }

    /**
     * This is not good case but if there is any kind of exception just return bad request and log exception
     * This should be divided into smaller exceptions with own handling mechanism
     *
     * @return ResponseEntity with BAD_REQUEST FLAG
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity handleException(Exception e) {
        log.error("Problem with handling form data.", e);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

}
