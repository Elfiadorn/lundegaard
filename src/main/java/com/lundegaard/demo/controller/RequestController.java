package com.lundegaard.demo.controller;

import com.lundegaard.demo.model.RequestType;
import com.lundegaard.demo.service.RequestTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/request")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RequestController {

    private final RequestTypeService requestTypeService;

    /**
     * url - host:port/request
     * method - get
     * @return Collection of Request types
     */
    @GetMapping()
    public Iterable<RequestType> retrieveRequestTypes() {
        log.info("Retrieving request types");
        return requestTypeService.getAllTypes();
    }

}
