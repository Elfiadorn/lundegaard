CREATE TABLE form_entity
(
    id UUID NOT NULL PRIMARY KEY,
    request_type_id UUID NOT NULL,
    policy_number int NOT NULL DEFAULT 0,
    name VARCHAR(50) NOT NULL DEFAULT '',
    surname VARCHAR(50) NOT NULL DEFAULT '',
    request VARCHAR(500) NOT NULL DEFAULT ''
);

ALTER TABLE form_entity ADD CONSTRAINT request_type_fk FOREIGN KEY (request_type_id) REFERENCES request_type;