CREATE TABLE request_type
(
    id UUID NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
    type VARCHAR(19) NOT NULL UNIQUE
);