# DEMO

## Prerequisites

Tested on:

PostgreSQL database
    
    Modify application.properties with own values


## Build

    mvn clean install

## Run

    mvn spring-boot:run
    
    http://localhost:8080/
    
For test purposes you can use data for postman 

    lundegaard.postman_collection.json
    
### How to

call 

    Get Request Types
    
copy any record into  

    Post Form -> Body -> requestType
    
and send request

### About

There are two results for saving data

Status: 

    200 Ok
    400 Bad Request
    
400 Bad Request - if data are not filled will return validation errors
in header. If there is any other error will only return this 400 code 
{ this can be modified }.

    
